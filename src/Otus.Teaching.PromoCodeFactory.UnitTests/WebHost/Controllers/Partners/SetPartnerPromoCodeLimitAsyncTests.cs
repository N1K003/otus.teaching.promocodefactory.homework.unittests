﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.DataBuilders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_NotFoundResult()
        {
            // Arrange
            var partnerId = Guid.Parse("E58D4343-B914-40EC-9B7C-1BDDB1C6EB33");
            var request = LimitRequestBuilder.CreateLimitRequest().WithOneMonthFutureEndDate().WithTenLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync((Partner) null);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerInactive_BadRequestResult()
        {
            // Arrange
            var partner = PartnerBuilder.CreatePartner().SetActive(false).WithInactiveLimits();
            var request = LimitRequestBuilder.CreateLimitRequest().WithOneMonthFutureEndDate().WithTenLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_NumberIssuedPromoCodesIsZero()
        {
            // Arrange
            var partner = PartnerBuilder.CreatePartner().SetActive().WithIssuedPromocodes(3).WithOneActiveLimit();
            var request = LimitRequestBuilder.CreateLimitRequest().WithOneMonthFutureEndDate().WithTenLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_DisablePreviousLimit()
        {
            // Arrange
            var partner = PartnerBuilder.CreatePartner().SetActive().WithOneActiveLimit();
            var previousLimitId = partner.PartnerLimits.Single().Id;
            var request = LimitRequestBuilder.CreateLimitRequest().WithOneMonthFutureEndDate().WithTenLimit();


            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            var previousLimit = partner.PartnerLimits.Single(x => x.Id == previousLimitId);
            previousLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitZero_BadRequestResult()
        {
            // Arrange
            var partner = PartnerBuilder.CreatePartner().SetActive().WithInactiveLimits();
            var request = LimitRequestBuilder.CreateLimitRequest().WithOneMonthFutureEndDate().WithZeroLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitLessThenZero_BadRequestResult()
        {
            // Arrange
            var partner = PartnerBuilder.CreatePartner().SetActive().WithInactiveLimits();
            var request = LimitRequestBuilder.CreateLimitRequest().WithOneMonthFutureEndDate().WithMinusOneLimit();
            request.Limit = -1;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_NewLimit_IsSavedInDb()
        {
            // Arrange
            var partner = PartnerBuilder.CreatePartner().SetActive().WithInactiveLimits();
            var request = LimitRequestBuilder.CreateLimitRequest().WithOneMonthFutureEndDate().WithTenLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            _partnersRepositoryMock.Verify(repo => repo.UpdateAsync(partner), Times.Once);
        }
    }
}