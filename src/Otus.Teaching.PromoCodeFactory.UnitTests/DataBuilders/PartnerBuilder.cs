﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.DataBuilders
{
    internal static class PartnerBuilder
    {
        public static Partner CreatePartner()
        {
            return new Partner
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true
            };
        }

        public static Partner SetActive(this Partner partner, bool active = true)
        {
            partner.IsActive = active;
            return partner;
        }

        public static Partner WithIssuedPromocodes(this Partner partner, int issuedPromocodesCount)
        {
            partner.NumberIssuedPromoCodes = issuedPromocodesCount;
            return partner;
        }

        public static Partner WithLimit(this Partner partner, PartnerPromoCodeLimit limit)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit> {limit};

            return partner;
        }

        public static Partner WithInactiveLimits(this Partner partner)
        {
            return partner.WithLimit(new PartnerPromoCodeLimit
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 07, 9),
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = 100
                }
            );
        }

        public static Partner WithOneActiveLimit(this Partner partner)
        {
            return partner.WithLimit(new PartnerPromoCodeLimit
            {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = DateTime.Today,
                EndDate = DateTime.Today.AddMonths(1),
                Limit = 100
            });
        }
    }
}