﻿using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.DataBuilders
{
    internal static class LimitRequestBuilder
    {
        public static SetPartnerPromoCodeLimitRequest CreateLimitRequest()
        {
            return new SetPartnerPromoCodeLimitRequest();
        }

        public static SetPartnerPromoCodeLimitRequest WithEndDate(this SetPartnerPromoCodeLimitRequest request, DateTime dateTime)
        {
            request.EndDate = dateTime;
            return request;
        }

        public static SetPartnerPromoCodeLimitRequest WithLimit(this SetPartnerPromoCodeLimitRequest request, int limit)
        {
            request.Limit = limit;
            return request;
        }

        public static SetPartnerPromoCodeLimitRequest WithOneMonthFutureEndDate(this SetPartnerPromoCodeLimitRequest request)
        {
            return request.WithEndDate(DateTime.Today.AddMonths(1));
        }

        public static SetPartnerPromoCodeLimitRequest WithTenLimit(this SetPartnerPromoCodeLimitRequest request)
        {
            return request.WithLimit(10);
        }

        public static SetPartnerPromoCodeLimitRequest WithZeroLimit(this SetPartnerPromoCodeLimitRequest request)
        {
            return request.WithLimit(0);
        }

        public static SetPartnerPromoCodeLimitRequest WithMinusOneLimit(this SetPartnerPromoCodeLimitRequest request)
        {
            return request.WithLimit(-1);
        }
    }
}